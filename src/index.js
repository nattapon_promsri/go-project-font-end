import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';
import MainPage from './page/MainPage';
import LoginPage from './page/LoginPage';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import GalleryPage from './page/GalleryPage';
import AddGalleryPage from './page/AddGalleryPage';
import SignUpPage from './page/SignUpPage';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { loginReducer } from './reducer/loginReducer';
import ImagesPage from './page/ImagePage';
import MyImagesPage from './page/MyImagesPage';

const store = createStore(loginReducer)

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/" component={MainPage} exact={true} />
        <Route path="/mainPage" component={MainPage} />
        <Route path="/loginPage" component={LoginPage} />
        <Route path="/galleryPage" component={GalleryPage} />
        <Route path="/addGalleryPage" component={AddGalleryPage} />
        <Route path="/signUpPage" component={SignUpPage} />
        <Route path="/imagesPage/:id" component={ImagesPage} />
        <Route path="/myImagesPage/:id" component={MyImagesPage} />
      </Switch>
    </BrowserRouter>
  </Provider>

  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
