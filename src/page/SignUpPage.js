import React, { useState } from 'react';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Row, Col, Button, Input, AutoComplete } from 'antd';
import '../page/MainPage.css';
import { signUp } from '../api';
import { host } from '../api/index';
import { useHistory } from 'react-router-dom';


const { Content } = Layout;


const SignUpPage = () => {


    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory()


    const onSubbmit = () => {

        signUp({Email : email, Password : password});
        setEmail("")
        setPassword("")
        history.push("/loginPage")
    }

    return (
        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">

                    <div className="site-card-border-less-wrapper">

                        <Row style={{ alignItems: "center" }} >
                            <Col span={8}>

                            </Col>
                            <Col span={8}>
                                <Card title="Sign Up" bordered={false} style={{ height: AutoComplete, width: AutoComplete, padding: 20, margin: 10, textAlign: "center" }}>
                                    <Input placeholder="Input your E-mail" style={{ margin: 10 }} value={email} onChange={(e) => setEmail(e.target.value)} />
                                    <Input placeholder="Input your Passwprd" type="password" value={password} style={{ margin: 10 }} onChange={(e) => setPassword(e.target.value)} />
                                    <Row>
                                        <Col span={4}>

                                        </Col>
                                        <Col span={16}>
                                            <Button style={{ margin: 10 }} onClick={onSubbmit}>Subbmit</Button>

                                        </Col>
                                        <Col span={4}>

                                        </Col>

                                    </Row>

                                </Card>
                            </Col>
                            <Col span={8}>

                            </Col>
                        </Row>
                    </div>

                </div>

            </Content>
        </Layout>
    )
}

export default SignUpPage;
