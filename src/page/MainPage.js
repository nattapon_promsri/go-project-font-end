import React, { useState, useEffect } from 'react';
import '../page/MainPage.css';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Row, Col, Input, Button, AutoComplete, List } from 'antd';
// import data from '../Data'
import { listGallery } from '../api';
// import axios from 'axios';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { host } from '../api/index';

const { Content } = Layout;
const { Search } = Input;


const MainPage = () => {

    // const [onSearchInPut, setOnSearchInPut] = useState("")
    const token = useSelector(state => state.token);
    const [allGallery, setAllGallery] = useState([]);
    // const [getOneGalleryImages, setGetOneGalleryImages] = useState([])

    useEffect(async () => {
        await listGallery()
            .then(res => {
                console.log("test: ", res.data)
                setAllGallery(res.data)
            })
            .catch(err => {
                console.error(err)
            })
    }, []);

    // const GetOneGallery = async (galleryID) => {

    //     await axios.get(`${host}/galleries/${galleryID}/images`)
    //         .then(response => {
    //             setGetOneGalleryImages(response.data)
    //         })
    //         .catch(err => {
    //             console.log("Error", err)
    //         });
    // }

    return (

        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}> All Gallery</Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">
                    <Row style={{ margin: 15 }}>
                        <Col span={6} >
                        </Col>
                        <Col span={12}>
                            {/* <Search
                                placeholder="Search Gallery Name"
                                enterButton="Search"
                                size="large"
                                onChange={(e) => setOnSearchInPut(e.target.value)}
                            /> */}
                        </Col>
                        <Col span={6} style={{ textAlign: "end" }}>

                        </Col>

                    </Row>

                    <div className="site-card-border-less-wrapper">
                        <List
                            grid={{
                                gutter: 16,
                                xs: 1,
                                sm: 2,
                                md: 4,
                                lg: 4,
                                xl: 3,
                                xxl: 3,
                            }}
                            dataSource={allGallery}
                            renderItem={item => (
                                <List.Item>
                                    <Card title={item.Name}>
                                        <a href={"/imagesPage/"+ item.ID}><Button > Watch Images</Button></a>
                                    </Card>
                                </List.Item>
                            )}
                        />
                    </div>
                </div>
            </Content>
        </Layout>
    )
}

export default MainPage;


{/* <Link to={{ pathname: `/imagesPage`
                                    , state: { galleryID: item.ID } 
                                    }}>
                                        <Card title={item.Name}>

                                            <label>{item.ID}</label>
                                        </Card>
                                    </Link> */}