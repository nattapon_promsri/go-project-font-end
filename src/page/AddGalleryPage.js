import React, { useState } from 'react';
import { Layout, Breadcrumb, Card, Input, Button, Row, Col, Upload } from 'antd';
import '../page/MainPage.css';
import Navbar from './Navbar';
import { setToken } from '../action/loginAction';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { UploadOutlined } from '@ant-design/icons';
import { host } from '../api/index';
import { useHistory } from 'react-router-dom';

const { Content } = Layout;


const AddGalleryPage = () => {


    const [galleryName, setGallery] = useState("")
    const [images, setImages] = useState([])
    const is_pubilc = true;
    const history = useHistory()

    const token = useSelector(state => state.token)

    const addGallery = async (e) => {

        await axios.post(`${host}/auth/galleries`, { name: galleryName, is_pubilc: is_pubilc }, {
            headers:
            {
                Authorization: `Bearer ${token}`,
            }
        })
            .then(response => {

                const id = response.data.id

                // const files = e.target.elements["photos"].files;
                const form = new FormData();
                for (let i = 0; i < images.length; i++) {
                    form.append("photos", images[i]);
                }
                axios.post(`${host}/auth/galleries/` + id + `/images`, form, {
                    headers:
                    {
                        Authorization: `Bearer ${token}`
                    }
                })
                    .then((response) => {
                        setImages([...images, ...response.data])
                        history.push("/galleryPage")
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            })
            .catch(err => {

                console.log("Error", err)

            });
    }

    return (
        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">
                    <div className="site-card-border-less-wrapper">

                        <Row>
                            <Col span={8} style={{ textAlign: "center" }}>
                                Gallery Name
                            </Col>

                            <Col span={8}>
                                <Input type="text" onChange={(e) => setGallery(e.target.value)} />
                            </Col>
                            <Col span={8}>
                                <Button onClick={addGallery}>Add</Button>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: 100 }}>
                            <Col span={6} >

                            </Col>
                            <Col span={12} style={{ textAlign: "center" }}>
                                <Upload multiple
                                    onChange={({ file, fileList, event }) => {

                                        setImages(fileList.map((f) => f.originFileObj));
                                    }}

                                >
                                    <Button>
                                        <UploadOutlined /> Upload
                                    </Button>
                                </Upload>
                            </Col>
                            <Col span={6} >

                            </Col>
                        </Row>

                    </div>
                </div>
            </Content>
        </Layout>
    )

}
export default AddGalleryPage;