import React, { useEffect, useState } from 'react';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Col, Row, List, Button } from 'antd';
import '../page/MainPage.css';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { host } from '../api/index';

const { Content } = Layout;


const GalleryPage = () => {

    const token = useSelector(state => state.token)
    const [myGallery, setMyGallery] = useState([])

    useEffect(() => {
        axios.get(`${host}/auth/galleries`, {
            headers:
            {
                Authorization: `Bearer ${token}`,
            }
        })
            .then(res => {
                setMyGallery(res.data)
            })
            .catch(err => {
                console.error(err)
            })

    }, [myGallery]);


    const deleteGallery = (galleryID) => {

        const id = galleryID
        axios.delete(`${host}/auth/galleries/` + id, {
            headers:
            {
                Authorization: `Bearer ${token}`,
            }
        })
            .then(res => {
                const filter = myGallery.filter((g) => g.galleryID !== id);
                setMyGallery(filter)
                console.log(res)
            })
            .catch(err => {
                console.error(err)
            })
    }

    const changeStatus = (galleryID, is_public) => {
        const id = galleryID
        const Status = !is_public

        axios.patch(`${host}/auth/galleries/` + id + `/publishes`, { is_public: Status }, {
            headers:
            {
                Authorization: `Bearer ${token}`,
            }
        })
            .then(res => {
                console.log(res)

            })
            .catch(err => {
                console.error(err)
            })
    }

    return (
        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">

                    <div className="site-card-border-less-wrapper">
                        <Row>
                            <Col span={6}>

                            </Col>
                            <Col span={12} style={{ textAlign: "center", marginBottom: 20 }}>
                                <h1>My Gallery</h1>
                            </Col>
                            <Col span={6} style={{ textAlign: "end" }}>
                                <Button type="primary" >
                                    <a href="/addGalleryPage">
                                        Add Gallery
                                </a>
                                </Button>
                            </Col>
                        </Row>
                        <List
                            grid={{
                                gutter: 12,
                                xs: 1,
                                sm: 2,
                                md: 4,
                                lg: 4,
                                xl: 3, // col
                                xxl: 3,
                            }}
                            dataSource={myGallery}
                            renderItem={item => (
                                <List.Item>
                                    <Card title={item.name}>
                                        <Row style={{ marginBottom: 30 }}>
                                            <Col >
                                                {item.is_public ?
                                                    <h3>Status : Public</h3>
                                                    :
                                                    <h3>Status : Pivate</h3>
                                                }</Col>
                                            <Col>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col span={8}>
                                                <Button style={{ margin: 10 }} > <a href={"/myImagesPage/"+ item.id}>Watch Images</a></Button>
                                            </Col >
                                            <Col span={8}>
                                                <Button style={{ margin: 10 }} onClick={() => changeStatus(item.id, item.is_public)}>Change Status</Button>
                                            </Col>
                                            <Col span={6}>
                                                <Button style={{ margin: 10 }} type="primary" danger onClick={() => deleteGallery(item.id)}>Delete</Button>
                                            </Col>
                                        </Row>
                                    </Card>
                                </List.Item>
                            )}
                        />

                    </div>

                </div>
            </Content>
        </Layout>
    )
}

export default GalleryPage;
