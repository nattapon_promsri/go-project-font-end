import React, { useEffect, useState } from 'react';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Row, Col, Button, AutoComplete, Upload } from 'antd';
import '../page/MainPage.css';
import axios from 'axios';
import { host } from '../api/index';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Modal from 'antd/lib/modal/Modal';
import { DeleteOutlined, UploadOutlined } from '@ant-design/icons';

const { Content } = Layout;


const MyImagesPage = (props) => {

    const [images, setImage] = useState([])
    const [oneGallery, setOneGallery] = useState([])
    const [modalValue, setModalValue] = useState(false)
    const [nameEdited, setNameEdited] = useState("")

    // const [uploadImages, setUploadImages] = useState([])


    const { id } = useParams()
    const token = useSelector(state => state.token)

    useEffect(() => {

        axios.get(`${host}/auth/galleries/` + id + `/images`, {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        })

            .then(res => {
                console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",res.data)
                setImage(res.data)
            })
            .catch(err => {
                console.log(err)
            })

        axios.get(`${host}/galleries/` + id)
            .then(response => {
                setOneGallery(response.data)
            })
            .catch(err => {
                console.log("Error", err)
            });
    }, [images]);



    const showModal = () => {
        setModalValue(true)
    }


    const handleOk = () => {
        axios.patch(`${host}/auth/galleries/` + id + `/names`, { name: nameEdited }, {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        })

            .then(res => {
                console.log(res)
                setModalValue(false)
            })
            .catch(err => {
                console.log(err)
            })
    }

    const handleCancel = () => {
        setModalValue(false)
    }

    const deleteImage = (id) => {

        const imageID = id

        axios.delete(`${host}/auth/images/` + imageID, {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            })


    }


    // const onUploadImage = () => {


    //     const form = new FormData();
    //     for (let i = 0; i < images.length; i++) {
    //         form.append("photos", images[i]);
    //     }
    //     axios.post(`${host}/auth/galleries/` + id + `/images`, form, {
    //         headers:
    //         {
    //             Authorization: `Bearer ${token}`
    //         }
    //     })
    //         .then((response) => {
    //             setUploadImages([...images, ...response.data])
    //         })
    //         .catch((err) => {
    //             console.error(err);
    //         });
    // }


    
    return (
        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">

                    <div className="site-card-border-less-wrapper">
                        <Row>
                            <Col span={6}>

                            </Col>
                            <Col span={12} style={{ textAlign: "center", marginBottom: 20 }}>
                                <Row>
                                    <Col span={6}>
                                    </Col>
                                    <Col span={12}>
                                        <h1>{oneGallery.name}</h1>
                                    </Col>
                                    <Col span={6}>
                                        <Button onClick={showModal}>Edit Name</Button>
                                    </Col>

                                </Row>

                            </Col>
                            <Col span={6} >

                            </Col>
                        </Row>
                        <Row>
                            {images.map((e, index) => {

                                return (
                                    <Row span={8}>
                                        <img
                                            style={{ height: 250, width: 350, margin: 10 }}
                                            src={`${host}/${e.filename}`}
                                        />
                                        <Button onClick={() => deleteImage(e.id)}><DeleteOutlined /></Button>
                                    </Row>
                                )
                            })}
                        </Row>
                        {/* <Row style={{ marginTop: 100 }}>
                            <Col span={6} >

                            </Col>
                            <Col span={12} style={{ textAlign: "center" }}>
                                <Upload multiple
                                    onChange={({ file, fileList, event }) => {

                                        setUploadImages(fileList.map((f) => f.originFileObj));
                                    }}

                                >
                                    <Button>
                                        <UploadOutlined /> Upload
                                    </Button>

                                </Upload>
                            </Col>
                            <Col span={6} >
                                <Button onClick={() => onUploadImage({ id, token })}>Submit</Button>
                            </Col>
                        </Row> */}

                    </div>
                    <Modal
                        title="Edit Name Gallery"
                        visible={modalValue}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <input type="text" value={nameEdited} onChange={(e) => setNameEdited(e.target.value)} style={{ width: AutoComplete }} />
                    </Modal>

                </div>
            </Content>
        </Layout>
    )
}

export default MyImagesPage;