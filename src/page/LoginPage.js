import React, { useState, useEffect } from 'react';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Row, Col, Button, Input, AutoComplete } from 'antd';
import '../page/MainPage.css';
import Modal from 'antd/lib/modal/Modal';
import { signIn } from '../api';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { setToken } from '../action/loginAction';
import { useHistory } from 'react-router-dom';
import { host } from '../api/index';

const { Content } = Layout;


const LoginPage = () => {

    // useEffect(async () => {

    //     const tokenLocal = localStorage.getItem("token")
    // console.log("get local ==================>",tokenLocal)
    // if (tokenLocal != null) {
    //     setToken(tokenLocal)
    // }

    // }, []);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const token = useSelector(state => state.token)

    const dispatch = useDispatch()
    const history = useHistory()

    const onLogin = async () => {

        await axios.post(`${host}/login`, { email, password })

            .then(response => {

                dispatch(setToken(response.data.token))
                localStorage.setItem("token", response.data.token)
                if (token != "" || token != null) {
                    setEmail("")
                    setPassword("")
                    history.push("/mainPage")
                }
            })
            .catch(err => {

                alert("Email or Password worng!!!")
                console.log("Error", err)


            });
    }

    return (
        <Layout className="layout">
            <Navbar />
            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">

                    <div className="site-card-border-less-wrapper">

                        <Row style={{ alignItems: "center" }} >
                            <Col span={8}>

                            </Col>
                            <Col span={8}>
                                <Card title="Sign In" bordered={false} style={{ height: AutoComplete, width: AutoComplete, padding: 20, margin: 10, textAlign: "center" }}>
                                    <Input placeholder="Input your E-mail" style={{ margin: 10 }} value={email} onChange={(e) => setEmail(e.target.value)} />
                                    <Input placeholder="Input your Passwprd" type="password" style={{ margin: 10 }} value={password} style={{ margin: 10 }} onChange={(e) => setPassword(e.target.value)} />
                                    <Row>
                                        <Col span={4}>

                                        </Col>
                                        <Col span={8}>
                                            <Button style={{ margin: 10 }} onClick={onLogin}>Sign In</Button>

                                        </Col>
                                        <Col span={8}>
                                            <Button style={{ margin: 10 }}>
                                                <a href="/signUpPage">
                                                    Sign Up
                                                </a>
                                            </Button>
                                        </Col>
                                        <Col span={4}>

                                        </Col>

                                    </Row>

                                </Card>
                            </Col>
                            <Col span={8}>

                            </Col>
                        </Row>
                    </div>

                </div>
            </Content>
        </Layout>
    )
}

export default LoginPage;
