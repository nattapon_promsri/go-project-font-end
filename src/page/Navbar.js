import React, { useState, useEffect } from 'react'
import { Layout, Menu, Row, Col, Button } from 'antd';
import { setToken } from '../action/loginAction';
import { useDispatch, useSelector } from 'react-redux';
import { host } from '../api/index';
import { useHistory } from 'react-router-dom';

const { Header } = Layout;

// const [showMyGallery, setshowMyGallery] = useState("false");


// useEffect(() => {
//     checkToken();
// }, []);


// const checkToken = () => {

//     const token = 4;

//     if (token != null || token != "") {
//         setshowMyGallery("true");
//     }
// }

const Navbar = () => {

    const dispatch = useDispatch()
    const token = useSelector(state => state.token)
    const history = useHistory()

    const onLogOut = () => {

        localStorage.removeItem("token")
        dispatch(setToken(""))
        history.push("/")
    }
    return (
        <Header>
            <Menu theme="dark" mode="horizontal" >
                <Row>
                    <Col span={2}>
                        <Menu.Item key="1"><a href="/mainPage">Home</a></Menu.Item>
                    </Col>
                    {
                        token == "" || token == null ?
                            <Col span={18}><Menu.Item ></Menu.Item> </Col>
                            : <Col span={18}>
                                <Menu.Item key="2"><a href="/galleryPage">My Gallery</a></Menu.Item>
                            </Col>
                    }
                    {
                        token == "" || token == null ?
                            <Col span={2} style={{ textAlign: "end" }}>
                                <Menu.Item key="3" ><a href="/loginPage">Login</a></Menu.Item>
                            </Col>
                            :
                            <Col span={2} style={{ textAlign: "center" }}>
                                <Menu.Item key="4" ><Button type="primary" danger onClick={onLogOut}>Log out</Button></Menu.Item>
                            </Col>
                    }
                </Row>
            </Menu>
        </Header>
    )
}


export default Navbar;