import React, { useEffect, useState } from 'react';
import Navbar from './Navbar';
import { Layout, Breadcrumb, Card, Row, Col } from 'antd';
import '../page/MainPage.css';
import axios from 'axios';
import { host } from '../api/index';
import { useParams } from 'react-router-dom';
import { List } from 'antd/lib/form/Form';

const { Content } = Layout;

const ImagesPage = () => {

    const [images, setImage] = useState([])
    const { id } = useParams()
    useEffect(() => {

        axios.get(`${host}/galleries/` + id + `/images`)
            .then(res => {
                console.log(res.data)
                setImage(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }, []);

    return (

        <Layout className="layout">

            <Navbar />

            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item style={{ margin: '16px 0' }}></Breadcrumb.Item>

                </Breadcrumb>
                <div className="site-layout-content">

                    <div className="site-card-border-less-wrapper">
                        <Row>
                            <Col span={6}>

                            </Col>
                            <Col span={12} style={{ textAlign: "center", marginBottom: 20 }}>
                                <h1>Photos</h1>
                            </Col>
                            <Col span={6} >

                            </Col>
                        </Row>
                        <Row>
                            {images.map((e, index) => {

                                return (
                                    // <image style={{ height: 250, width: 250 }} >{e.Filename}</image>
                                    <Col span={8}>
                                        <img
                                            style={{ height: 250, width: 400, margin: 10 }}
                                            src={`${host}/upload/${id}/${e.Filename}`}
                                        />
                                    </Col>
                                )

                            })}
                        </Row>
                    </div>

                </div>
            </Content>
        </Layout>
    )
}

export default ImagesPage;