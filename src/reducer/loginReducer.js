const getToken = () =>{
  return localStorage.getItem("token")||""
}
const initialState = {
    token: getToken(),
}

export const loginReducer = (state = initialState, action) => {

    switch (action.type) {
        case 'SET_TOKEN':
            return {
                ...initialState,
                token: action.token,
            }
        default:
            return initialState
    }
} 